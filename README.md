# we-dial-card

> Western Electric 500 dial card maker.

## Build Setup

``` bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm start

# generate static project
$ npm run generate

# upload to s3
$ aws s3 cp dist s3://<bucket_name> --recursive --profile=<profile_name>
```

For detailed explanation on how things work, checkout [Nuxt.js docs](https://nuxtjs.org).
